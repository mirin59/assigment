import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Assignment {
    private JPanel root;
    private JLabel topLabel;
    private JButton karaage;
    private JButton yakisaba;
    private JButton chikinkatu;
    private JButton syougayaki;
    private JButton yasaiitame;
    private JButton nasumiso;
    private JButton RiceSizeL;
    private JButton PorkSoup;
    private JTextPane orderedItemsList;
    private JTextPane totalPrice;
    private JButton checkOut;
    private JButton RiceSizeS;
    private JLabel orderedItem;
    int sum_price=0;
    void order(String food,int price){
        int confirmation=JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,"Thank you ordering "+food+"!");
            String currentText=orderedItemsList.getText();
            orderedItemsList.setText(currentText+food+" "+price+"yen\n");
            sum_price+=price;
            totalPrice.setText("Total "+sum_price+" yen");

        }
    }
    void plusOrder(String change,int cost){
        int confirmation=JOptionPane.showConfirmDialog(null,
                "Would you like to change "+change+"?(It cost "+cost+" yen)",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,"Thank you!Changed "+change+"!");
            String currentText=orderedItemsList.getText();
            orderedItemsList.setText(currentText+"-Changed "+change+"("+cost+"yen)\n");
            sum_price+=cost;
            totalPrice.setText("Total "+sum_price+" yen");
        }

    }
    public Assignment() {
        karaage.setIcon(new ImageIcon(this.getClass().getResource("karaage.jpg")));

        yakisaba.setIcon(new ImageIcon(this.getClass().getResource("yakisaba.jpg")));

        syougayaki.setIcon(new ImageIcon(this.getClass().getResource("syougayaki.jpg")));

        chikinkatu.setIcon(new ImageIcon(this.getClass().getResource("chikinkatu.jpg")));

        yasaiitame.setIcon(new ImageIcon(this.getClass().getResource("yasaiitame.jpg")));

        nasumiso.setIcon(new ImageIcon(this.getClass().getResource("nasumiso.jpg")));


        karaage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",600);
            }
        });
        yakisaba.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisaba",550);
            }
        });
        chikinkatu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chikinkatu",700);
            }
        });
        syougayaki.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Syougayaki",750);
            }
        });
        yasaiitame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yasaiitame",500);

            }
        });
        nasumiso.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("NasuMiso",800);

            }
        });
        RiceSizeL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              plusOrder("the rice to L size",50);

            }
        });
        RiceSizeS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                plusOrder("the rice to S size",-50);
            }
        });
        PorkSoup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                plusOrder("miso soup to pork soup",100);

            }
        });

        checkOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "CheckOut Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you!The total price is "+sum_price+"yen!");
                    orderedItemsList.setText("");
                    sum_price=0;
                    totalPrice.setText("Total "+sum_price+" yen");
                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
